#include <HD44780.h>
#include <RotaryEncoder.h>
#include <ACTUI.h>
#include <ACTUIMenuRTC.h>
#include <ACTUIMenuSystem.h>
#include <TaskOverInterrupt.h>

void mMain1(void) {
        HD44780Putsf(PSTR("Main1"));
}

void mMain2(void) {
        HD44780Putsf(PSTR("Main2"));
}

void mAkce2(void) {
        HD44780Putsf(PSTR("Akce2"));
}

void mMain3(void) {
        HD44780Putsf(PSTR("Main3"));
}

void mAkce3(void);

const MenuFunction PROGMEM menuMain[] = {
        itemRTC,menuRTC,
        itemSystem,menuSystem,
        mMain2,mAkce2,
        mMain3,mAkce3,
        0x0000
};

void mSub1(void) {
        HD44780Putsf(PSTR("Sub1"));
}

void mSubAkce1(void) {
        HD44780Putsf(PSTR("SubAkce1"));
}

void mSub2(void) {
        HD44780Putsf(PSTR("Sub2"));
}

void mSub3(void) {
        HD44780Putsf(PSTR("Sub3"));
}

void mSubAkce3(void) {
        HD44780Putsf(PSTR("SubAkce3"));
}

const MenuFunction PROGMEM menuSub[] = {
        0x0000,0x0000,
        mSub1,mSubAkce1,
        mSub2,0x0000,
        mSub3,mSubAkce3,
        0x0000
};

void mAkce3(void) {
        menu(menuSub);
}

void everyTimeTask(void) {
        static uint16_t backlighting = 0;
        if (rotaryEncoderChanged()) {
                backlighting = 2500;
        }
        if (backlighting) {
                backlighting--;
                HD44780BacklightingOn();
        } else {
                HD44780BacklightingOff();
        }
}

void setup() {
        HD44780Init();
        HD44780GotoXY(0,0);
        HD44780Cursor(0);
        rotaryEncoderInit();
        rotaryEncoderXMax = 32767;
        rotaryEncoderXMin = -32767;
        TOIInit(FREQUENCY2DIVIDER(125), everyTimeTask, 0);
}

void loop() {
        menu(menuMain);
}
