#ifndef _ACTUIMenuRTC_H
#define _ACTUIMenuRTC_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void menuRTC(void);
/**
 * Nacte a robrazi cas primo z DS1302. Pak ceka 100 ms.
 */
void itemRTC(void);

#ifdef __cplusplus
}
#endif

#endif /* _ACTUIMenuRTC_H */
