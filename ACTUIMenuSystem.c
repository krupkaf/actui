#include "DS1302.h"
#include "TimeTools.h"
#include <ACTUI.h>
#include <Arduino.h>
#include <HD44780.h>
#include <avr/wdt.h>
#include <util/delay.h>

uint8_t uptimeType = 0;
time_t startTime = 0;

void printUptimeHours(time_t t) {
  char s[9];
  int32str(t / 36UL, s, 8, 2);
  HD44780GotoXY(2, 1);
  HD44780Puts(s);
  HD44780Putsf(PSTR(" hod"));
}

void printUptimeFormat(time_t t) {
  char s[12];
  struct tm TM;
  gmtime(t, &TM);
  int32str(TM.tm_yday, s, 4, -1);
  s[4] = ',';
  int32str(TM.tm_hour, &s[5], 2, -1);
  if (s[5] == ' ')
    s[5] = '0';
  s[7] = ':';
  int32str(TM.tm_min, &s[8], 2, -1);
  if (s[8] == ' ')
    s[8] = '0';
  s[10] = ':';
  int32str(TM.tm_sec, &s[11], 2, -1);
  if (s[11] == ' ')
    s[11] = '0';
  HD44780GotoXY(1, 1);
  HD44780Puts(s);
}

void iUptime(void) {
  switch (uptimeType) {
  case 0:
    HD44780Putsf(PSTR("Arduino uptime"));
    printUptimeHours(millis() / 1000UL);
    break;
  case 1:
    HD44780Putsf(PSTR("Arduino uptime"));
    printUptimeFormat(millis() / 1000UL);
    break;
  case 2:
  case 3:
    HD44780Putsf(PSTR("RTC uptime"));
    {
      struct tm TM;
      time_t t;
      DS1302ReadTimeCli(&TM);
      t = mktime(&TM) - startTime;
      switch (uptimeType) {
      case 2:
        printUptimeHours(t);
        break;
      case 3:
        printUptimeFormat(t);
        break;
      }
    }
    break;
  }
  _delay_ms(100);
}

void aUptime(void) {
  uptimeType++;
  if (uptimeType > 3) {
    uptimeType = 0;
  }
}

void iReset(void) { HD44780Putsf(PSTR("Reset")); }

void aReset(void) {
  int8_t a = 0;
  formattedEdit8(&a, -127, 127, viewInt16);
  if (a == 1) {
    HD44780Clear();
    HD44780Putsf(PSTR(" * Reset * "));
    wdt_disable();
    cli();
    _delay_ms(2000);
    asm("JMP 0x00");
  };
}

void iBuild(void) {
  HD44780Putsf(PSTR("Build  "__TIME__));
  HD44780GotoXY(0, 1);
  HD44780Putsf(PSTR(" "__DATE__));
}

const MenuFunction PROGMEM subMenuSystem[] = {
    0x0000, 0x0000, iUptime, aUptime, iReset, aReset, iBuild, 0x0000, 0x0000};

void itemSystem(void) { HD44780Putsf(PSTR("   System")); }

void menuSystem(void) { menu(subMenuSystem); }

void __attribute__((constructor)) my_init(void) {
  struct tm TM;
  DS1302ReadTimeCli(&TM);
  startTime = mktime(&TM);
};
