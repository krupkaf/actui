#include "DS1302.h"
#include "TimeTools.h"
#include <ACTUI.h>
#include <HD44780.h>
#include <util/delay.h>

struct tm t;
uint8_t update;

void mYear(void) {
  HD44780Putsf(PSTR("Rok"));
  viewInt16(t.tm_year);
}

void mMonth(void) {
  HD44780Putsf(PSTR("Mesic"));
  viewInt16(t.tm_mon + 1);
}

void mDay(void) {
  HD44780Putsf(PSTR("Den"));
  viewInt16(t.tm_mday);
}

void mHour(void) {
  HD44780Putsf(PSTR("Hodina"));
  viewInt16(t.tm_hour);
}

void mMin(void) {
  HD44780Putsf(PSTR("Minuta"));
  viewInt16(t.tm_min);
}

void mSet(void) { HD44780Putsf(PSTR("Nastavit")); }

void aYear(void) {
  formattedEdit8((int8_t *)&t.tm_year, 0, 99, viewInt16);
  update = 0;
}

void aMonth(void) {
  t.tm_mon++;
  formattedEdit8((int8_t *)&t.tm_mon, 1, 12, viewInt16);
  t.tm_mon--;
  update = 0;
}

void aDay(void) {
  formattedEdit8((int8_t *)&t.tm_mday, 1, 31, viewInt16);
  update = 0;
}

void aHour(void) {
  formattedEdit8((int8_t *)&t.tm_hour, 0, 23, viewInt16);
  update = 0;
}

void aMin(void) {
  formattedEdit8((int8_t *)&t.tm_min, 0, 59, viewInt16);
  update = 0;
}

void aSet(void) {
  t.tm_sec = 0;
  gmtime(mktime(&t), &t); // Prevedu cas na time_t a zpet, aby se nastavylo wday
  DS1302WriteTimeCli(&t);
  HD44780Clear();
  HD44780Putsf(PSTR(" OK"));
  update = 1;
}

void itemRTCm(void) {
  if (update) {
    DS1302ReadTimeCli(&t);
  }
  printTime(&t);
  _delay_ms(100);
}

const MenuFunction PROGMEM subMenuRTC[] = {
    0x0000, 0x0000, itemRTCm, 0x0000, mYear, aYear, mMonth, aMonth, mDay,
    aDay,   mHour,  aHour,    mMin,   aMin,  mSet,  aSet,   0x0000};

void menuRTC(void) {
  update = 1;
  DS1302ReadTime(&t);
  menu(subMenuRTC);
}

void itemRTC(void) {
  DS1302ReadTimeCli(&t);
  printTime(&t);
  _delay_ms(100);
}
