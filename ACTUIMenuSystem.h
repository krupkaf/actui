#ifndef _ACTUIMenuSystem_H
#define _ACTUIMenuSystem_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void menuSystem(void);
void itemSystem(void);

#ifdef __cplusplus
}
#endif

#endif /* _ACTUIMenuSystem_H */
