#ifndef _ACTUI_H
#define _ACTUI_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "TimeTools.h"

typedef enum {
  Auto = -1,
  Off = 0,
  On = 1,
} TReleState;

void int32str(signed long c, char *buf, char s, signed char d);
void int16str(signed short c, char *buf, char l, signed char d);

void printInt32(int32_t t);
void printInt16(int16_t t);
void printInt16AsF1(int16_t t);
void printInt16AsF2(int16_t t);
void printInt16AsF4(int16_t t);
void printInt8(int8_t t);
void printUInt8(uint8_t t);
void printUInt8hex(uint8_t t);
void printTemperature(int16_t t);
void printTemperature16(int16_t t);
void printReleState(TReleState req, TReleState real);
void printTimeHHMM(int16_t minutes);
void printTimeHHMMSS(time_t t);
void printTimeHHMMSS16(int16_t t);
/**
 * Vypis kompletniho casu a data.
 * @param t
 */
void printTime(struct tm *t);
/**
 * Vypis kompletniho casu a data.
 * @param t
 */
void printTimeU(time_t t);

void viewInt16(int16_t i);
void viewReleState(int16_t i);
void viewInt16Sec(int16_t t);
void viewTimeHHMM(int16_t minutes);
void viewUInt8Hex(int16_t i);
void viewUInt8(int16_t t);

/**
 * convert tm to string hh:mm:ss
 */
void timestr(struct tm *tmbuf, char *str);
/**
 * convert tm to string dd dd.mm.yy
 */
void datestr(struct tm *tmbuf, char *str);

typedef void(FormattingFunctions)(int16_t);
void formattedEdit(int16_t *value, int16_t min, int16_t max,
                   FormattingFunctions fFun);
void formattedEdit8(int8_t *value, int8_t min, int8_t max,
                    FormattingFunctions fFun);
void formattedEditRele(TReleState *value);

typedef void (*MenuFunction)(void);
/**
 * Zobdazeni menu
 *
 * Maximalne je mozne mit v menu 254 polozek.
 * Sude polozky pole functions, vcetne 0-te, odkazuji na funkce, ktere se
 * zobrazuji polozky menu. Nasledujici liche funkce jsou volany jako akce menu.
 * Pokud je na pozici 0 pole functions hodnota 0, zobrazi se '..'.
 * Pokud je na pozici akce pole functions hodnota 0, pri vyvolani teto akce se
 * ukonci funkce menu.
 *
 * @param functions Pole funkci polozek menu ve FLASH zakoncene 0x0000 na miste
 * polozky menu(suda pozice). 0x0000 na 0-te pozici neni brana jako konec pole.
 */
void menu(const MenuFunction *functions);

#ifdef __cplusplus
}
#endif

#endif /* _ACTUI_H */
