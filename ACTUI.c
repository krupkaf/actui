#include <ACTUI.h>
#include <HD44780.h>
#include <RotaryEncoder.h>
#include <util/delay.h>

/**
 * prevede cislo c na retezec buf o delce s, s desetinou teckou na miste d
 * od konce retezce, pkud d==-1 tecka se nekona
 *
 * @param c   prevadene cislo
 * @param buf buffer pro vysledek
 * @param s   delka vysledneho retezce
 * @param d   pozice desetinne tecky
 */
void int32str(signed long c, char *buf, char s, signed char d) {
  char n = 0;
  signed char sgn = 0; // Znamenko 0 - kladne, 1 - zaporne
  char tmp;

  if (c < 0) {
    c = -c;
    sgn = 1;
  };
  buf = buf + s;
  *buf = 0; // Konec retezce
  for (; s; s--) {
    if (d == 0) {
      tmp = '.';
    } else {
      tmp = (c % 10) + '0';
      if ((tmp == '0') && (n == 1) && (c == 0)) {
        if (sgn == 1) {
          tmp = '-';
          sgn = 0; // Zapomenu na to, ze zpracovavam zaporne cislo (jedno minus
                   // staci).
        } else {
          tmp = ' ';
        };
      };
      c = c / 10;
      if (d < 0)
        n = 1;
    };
    d--;
    buf--;
    *buf = tmp;
  };
}

void int16str(signed short c, char *buf, char l, signed char d) {
  int32str(c, buf, l, d);
}

void char2str(signed char c, char *buf, char l, signed char d) {
  int32str(c, buf, l, d);
}

void printInt32(int32_t t) {
  char s[12];
  int32str(t, s, 11, -1);
  HD44780Puts(s);
}

void printInt16(int16_t t) {
  char s[7];
  int16str(t, s, 6, -1);
  HD44780Puts(s);
}

void printInt8(int8_t t) {
  char s[5];
  int16str(t, s, 4, -1);
  HD44780Puts(s);
}

void printUInt8(uint8_t t) {
  char s[5];
  int16str(t, s, 3, -1);
  HD44780Puts(s);
}

void printUInt4hex(uint8_t t) {
  register char a;
  t &= 0x0f;
  if (t > 9) {
    a = t + 'A' - 10; 
  } else {
    a = t + '0';
  }
  HD44780PutChar(a);
}

void printUInt8hex(uint8_t t) {
  printUInt4hex(t >> 4);
  printUInt4hex(t);
}

void printTemperature(int16_t t) {
  char s[7];
  HD44780GotoXY(4, 1);
  int16str(t, s, 6, 1);
  HD44780Puts(s);
  HD44780PutChar(223);
  HD44780PutChar('C');
}

void printTemperature16(int16_t t) {
  t = (t * 5) >> 3;
  if (t & 0x1000)
    t |= 0xf000;
  printTemperature(t);
}

void printReleState(TReleState req, TReleState real) {
  HD44780GotoXY(4, 1);
  HD44780PutChar((int8_t)req == -1 ? 'A' : req + '0');
  HD44780PutChar(' ');
  HD44780PutChar(126);
  HD44780PutChar(' ');
  if (real > 1)
    real = 1;
  HD44780PutChar('0' + real);
}

void printTimeHHMM(int16_t minutes) {
  HD44780GotoXY(4, 1);
  viewTimeHHMM(minutes);
}

void printTimeHHMMSS(time_t t) {
  char s[4];
  printTimeHHMM(t / 60);
  s[0] = ':';
  int16str(t % 60, &s[1], 2, -1);
  if (s[1] == ' ') {
    s[1] = '0';
  }
  HD44780Puts(s);
}

void printTimeHHMMSS16(int16_t t) {
    printTimeHHMMSS(t);
}

void printTime(struct tm *t) {
  char s[16];
  HD44780GotoXY(4, 0);
  timestr(t, s);
  HD44780Puts(s);
  HD44780GotoXY(2, 1);
  datestr(t, s);
  HD44780Puts(s);
}

void printTimeU(time_t t) {
  struct tm tmbuf;
  gmtime(t, &tmbuf);
  printTime(&tmbuf);
}

void viewInt16(int16_t i) {
  HD44780GotoXY(3, 1);
  printInt16(i);
}

void viewUInt8Hex(int16_t i) {
  HD44780GotoXY(7, 1);
  printUInt8hex(i);  
}

void viewUInt8(int16_t t)  {
  HD44780GotoXY(7, 1);
  printUInt8(t);  
}

void printInt16AsF1(int16_t t) {
  char s[9];
  HD44780GotoXY(2, 1);
  int32str(t, s, 8, 1);
  HD44780Puts(s);
}

void printInt16AsF2(int16_t t) {
  char s[9];
  HD44780GotoXY(2, 1);
  int32str(t, s, 8, 2);
  HD44780Puts(s);
}

void printInt16AsF4(int16_t t) {
  char s[9];
  HD44780GotoXY(2, 1);
  int32str(t, s, 8, 4);
  HD44780Puts(s);
}

void viewInt16Sec(int16_t t) {
  viewInt16(t);
  HD44780Putsf(PSTR(" s"));
}

void viewTimeHHMM(int16_t minutes) {
  char s[7];  
  int16str(minutes / 60, s, 2, -1);
  s[2] = ':';
  int16str(minutes % 60, &s[3], 2, -1);
  if (s[3] == ' ') {
    s[3] = '0';
  }
  HD44780Puts(s);
}

void viewReleState(int16_t i) {
  HD44780GotoXY(4, 1);
  if (i == -1) {
    HD44780Putsf(PSTR("Auto"));
    return;
  }
  HD44780Putsf(PSTR("   "));
  HD44780PutChar(i + '0');
}

void timestr(struct tm *tmbuf, char *str) {
  int16str(tmbuf->tm_hour, str, 2, -1);
  str += 2;
  *str = ':';
  str++;
  int16str(tmbuf->tm_min, str, 2, -1);
  if (*str == ' ')
    *str = '0';
  str += 2;
  *str = ':';
  str++;
  int16str(tmbuf->tm_sec, str, 2, -1);
  if (*str == ' ')
    *str = '0';
}

const char PROGMEM sday[8][2] = {"Ne", "Po", "Ut", "St", "Ct", "Pa", "So"};

void datestr(struct tm *tmbuf, char *str) {
  *str = pgm_read_byte(&sday[tmbuf->tm_wday][0]);
  str++;
  *str = pgm_read_byte(&sday[tmbuf->tm_wday][1]);
  str++;
  *str = ' ';
  str++;
  int16str(tmbuf->tm_mday, str, 2, -1);
  str += 2;
  *str = '.';
  str++;
  int16str(tmbuf->tm_mon + 1, str, 2, -1);
  str += 2;
  *str = '.';
  str++;
  *str = ' ';
  str++;
  int16str(tmbuf->tm_year, str, 2, -1);
  if (*str == ' ')
    *str = '0';
}

void formattedEdit(int16_t *value, int16_t min, int16_t max,
                   FormattingFunctions fFun) {
  int16_t x1;
  uint8_t a = 0;

  HD44780ClearLine(1);
  HD44780GotoXY(0, 1);
  HD44780PutChar(0x3e);

  rotaryEncoderWaitToUp();
  rotaryEncoderMinMax(min, max);
  rotaryEncoderSet(*value);
  for (;;) {
    x1 = rotaryEncoderGet();
    fFun(x1);
    if (rotaryEncoderButton) {
      break;
    }
    _delay_ms(50);
  };
  while (rotaryEncoderButton) {
    _delay_ms(10);
    if (a > 250) {
      break;
    }
    a++;
  };
  if (a <= 250) {
    *value = x1;
  } else {
    HD44780ClearLine(1);
    fFun(*value);
  }
}

void formattedEdit8(int8_t *value, int8_t min, int8_t max,
                    FormattingFunctions fFun) {
  int16_t a = *value;
  formattedEdit(&a, min, max, fFun);
  *value = a;
}

void formattedEditRele(TReleState *value) {
  formattedEdit8((int8_t *)value, -1, 1, viewReleState);
}

void menu(const MenuFunction *functions) {
  // Pocet polozek menu - 1. (Maximalni index)
  uint8_t menuiMax;
  // Index prave zpracovavana polozka menu
  uint8_t menui;

  uint8_t menui2;

  MenuFunction f;

  menuiMax = 1;
  while (1) {
    MenuFunction f;
    memcpy_P(&f, &(functions[menuiMax << 1]), sizeof(MenuFunction));
    if (f == (MenuFunction)0x0000) {
      menuiMax--;
      break;
    }
    menuiMax++;
  }

  rotaryEncoderWaitToUp();

  rotaryEncoderSet(0);

  menui = 255;
  for (;;) {
    menui2 = rotaryEncoderGet();
    if (menui != menui2) {
      rotaryEncoderMinMax(0, menuiMax);
      HD44780Clear();
      menui = menui2;
      memcpy_P(&f, &(functions[menui << 1]), sizeof(MenuFunction));
    };
    HD44780GotoXY(0, 0);
    if (f == (MenuFunction)0x0000) {
      HD44780Putsf(PSTR(".."));
    } else {
      f();
    }
    if (rotaryEncoderButton) {
      memcpy_P(&f, &(functions[(menui << 1) + 1]), sizeof(MenuFunction));
      if (f != (MenuFunction)0x0000) {
        f();
      }
      rotaryEncoderWaitToUp();
      if (f == (MenuFunction)0x0000) {
        return;
      }
      rotaryEncoderSet(menui);
      menui = 255;
    }
  };
}
